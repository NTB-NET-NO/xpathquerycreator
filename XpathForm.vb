Imports System.io
Imports System.windows.forms
Imports System.Xml

Public Class XPathForm
    Inherits Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ResultList As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Hits As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TotalFiles As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents SampleDir As System.Windows.Forms.TextBox
    Friend WithEvents CompareType As System.Windows.Forms.ComboBox
    Friend WithEvents Query As System.Windows.Forms.TextBox
    Friend WithEvents FileHits As System.Windows.Forms.TextBox
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Progress As System.Windows.Forms.ProgressBar
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DBList As System.Windows.Forms.ComboBox
    Friend WithEvents DBConnection As System.Data.SqlClient.SqlConnection
    Friend WithEvents XPathAdapter As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents GetXPathSqlCommand As System.Data.SqlClient.SqlCommand
    Friend WithEvents XPathDataSet As XPathQueryCreator.XPathDataSet
    Friend WithEvents Status As System.Windows.Forms.Label
    Friend WithEvents Subs As System.Windows.Forms.CheckBox
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents TestBtn As System.Windows.Forms.Button
    Friend WithEvents BrowseBtn As System.Windows.Forms.Button
    Friend WithEvents AbortBtn As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ResultList = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader8 = New System.Windows.Forms.ColumnHeader
        Me.Query = New System.Windows.Forms.TextBox
        Me.TestBtn = New System.Windows.Forms.Button
        Me.Hits = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.CompareType = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.SampleDir = New System.Windows.Forms.TextBox
        Me.BrowseBtn = New System.Windows.Forms.Button
        Me.TotalFiles = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.FileHits = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.AbortBtn = New System.Windows.Forms.Button
        Me.Progress = New System.Windows.Forms.ProgressBar
        Me.Label8 = New System.Windows.Forms.Label
        Me.DBList = New System.Windows.Forms.ComboBox
        Me.XPathDataSet = New XPathQueryCreator.XPathDataSet
        Me.DBConnection = New System.Data.SqlClient.SqlConnection
        Me.XPathAdapter = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.GetXPathSqlCommand = New System.Data.SqlClient.SqlCommand
        Me.Status = New System.Windows.Forms.Label
        Me.Subs = New System.Windows.Forms.CheckBox
        CType(Me.Hits, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XPathDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ResultList
        '
        Me.ResultList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ResultList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader7, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader8})
        Me.ResultList.FullRowSelect = True
        Me.ResultList.GridLines = True
        Me.ResultList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ResultList.Location = New System.Drawing.Point(8, 184)
        Me.ResultList.MultiSelect = False
        Me.ResultList.Name = "ResultList"
        Me.ResultList.Size = New System.Drawing.Size(944, 512)
        Me.ResultList.TabIndex = 0
        Me.ResultList.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Keyword"
        Me.ColumnHeader1.Width = 174
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Timestamp"
        Me.ColumnHeader7.Width = 117
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Tjeneste"
        Me.ColumnHeader4.Width = 101
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Stoffgruppe"
        Me.ColumnHeader5.Width = 99
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Undergruppe"
        Me.ColumnHeader6.Width = 102
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Nodehits"
        Me.ColumnHeader2.Width = 61
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Filename"
        Me.ColumnHeader3.Width = 262
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Full path"
        Me.ColumnHeader8.Width = 0
        '
        'Query
        '
        Me.Query.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Query.Location = New System.Drawing.Point(8, 96)
        Me.Query.Multiline = True
        Me.Query.Name = "Query"
        Me.Query.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Query.Size = New System.Drawing.Size(560, 64)
        Me.Query.TabIndex = 1
        Me.Query.Text = ""
        '
        'TestBtn
        '
        Me.TestBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TestBtn.Location = New System.Drawing.Point(848, 128)
        Me.TestBtn.Name = "TestBtn"
        Me.TestBtn.Size = New System.Drawing.Size(104, 23)
        Me.TestBtn.TabIndex = 2
        Me.TestBtn.Text = "Test XPath"
        '
        'Hits
        '
        Me.Hits.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Hits.Location = New System.Drawing.Point(664, 128)
        Me.Hits.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.Hits.Name = "Hits"
        Me.Hits.Size = New System.Drawing.Size(72, 20)
        Me.Hits.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label1.Location = New System.Drawing.Point(576, 128)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 20)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Required hits:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label2.Location = New System.Drawing.Point(576, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 20)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Compare type:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CompareType
        '
        Me.CompareType.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CompareType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CompareType.Items.AddRange(New Object() {"Greater than: >", "Equal: =", "Less than: <", "Not equal: != / <>"})
        Me.CompareType.Location = New System.Drawing.Point(664, 96)
        Me.CompareType.Name = "CompareType"
        Me.CompareType.Size = New System.Drawing.Size(168, 21)
        Me.CompareType.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label3.Location = New System.Drawing.Point(8, 163)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(240, 20)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Results:  (XML files that match query)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label4.Location = New System.Drawing.Point(8, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 24)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "XML samples folder:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SampleDir
        '
        Me.SampleDir.Location = New System.Drawing.Point(136, 8)
        Me.SampleDir.Name = "SampleDir"
        Me.SampleDir.ReadOnly = True
        Me.SampleDir.Size = New System.Drawing.Size(312, 20)
        Me.SampleDir.TabIndex = 9
        Me.SampleDir.Text = ""
        '
        'BrowseBtn
        '
        Me.BrowseBtn.Location = New System.Drawing.Point(456, 8)
        Me.BrowseBtn.Name = "BrowseBtn"
        Me.BrowseBtn.Size = New System.Drawing.Size(112, 20)
        Me.BrowseBtn.TabIndex = 10
        Me.BrowseBtn.Text = "Browse ..."
        '
        'TotalFiles
        '
        Me.TotalFiles.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TotalFiles.Location = New System.Drawing.Point(880, 8)
        Me.TotalFiles.Name = "TotalFiles"
        Me.TotalFiles.ReadOnly = True
        Me.TotalFiles.Size = New System.Drawing.Size(72, 20)
        Me.TotalFiles.TabIndex = 11
        Me.TotalFiles.Text = ""
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label5.Location = New System.Drawing.Point(744, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 23)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Total XML-files in sample:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FileHits
        '
        Me.FileHits.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FileHits.Location = New System.Drawing.Point(880, 160)
        Me.FileHits.Name = "FileHits"
        Me.FileHits.ReadOnly = True
        Me.FileHits.Size = New System.Drawing.Size(72, 20)
        Me.FileHits.TabIndex = 13
        Me.FileHits.Text = ""
        '
        'Label6
        '
        Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label6.Location = New System.Drawing.Point(8, 80)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(336, 16)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "XPath query to test against XML selection:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label7.Location = New System.Drawing.Point(720, 163)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(152, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Total XML matches:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'AbortBtn
        '
        Me.AbortBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AbortBtn.Enabled = False
        Me.AbortBtn.Location = New System.Drawing.Point(848, 96)
        Me.AbortBtn.Name = "AbortBtn"
        Me.AbortBtn.Size = New System.Drawing.Size(104, 23)
        Me.AbortBtn.TabIndex = 16
        Me.AbortBtn.Text = "Abort"
        '
        'Progress
        '
        Me.Progress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Progress.Location = New System.Drawing.Point(8, 701)
        Me.Progress.Name = "Progress"
        Me.Progress.Size = New System.Drawing.Size(832, 20)
        Me.Progress.Step = 1
        Me.Progress.TabIndex = 17
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(8, 53)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 16)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Select from database:"
        '
        'DBList
        '
        Me.DBList.DataSource = Me.XPathDataSet
        Me.DBList.DisplayMember = "GroupXpath.XpathName"
        Me.DBList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DBList.Location = New System.Drawing.Point(136, 49)
        Me.DBList.Name = "DBList"
        Me.DBList.Size = New System.Drawing.Size(432, 21)
        Me.DBList.TabIndex = 19
        Me.DBList.ValueMember = "GroupXpath.GroupXpathID"
        '
        'XPathDataSet
        '
        Me.XPathDataSet.DataSetName = "XPathDataSet"
        Me.XPathDataSet.Locale = New System.Globalization.CultureInfo("nb-NO")
        '
        'DBConnection
        '
        Me.DBConnection.ConnectionString = "workstation id=""GX270-LCH"";packet size=4096;user id=ntb_dist;data source=MELBOURN" & _
        "E;persist security info=True;initial catalog=ntb_distribution;password=xmldist"
        '
        'XPathAdapter
        '
        Me.XPathAdapter.SelectCommand = Me.SqlSelectCommand1
        Me.XPathAdapter.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "GroupXpath", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("GroupXpathID", "GroupXpathID"), New System.Data.Common.DataColumnMapping("XpathName", "XpathName"), New System.Data.Common.DataColumnMapping("XPath", "XPath"), New System.Data.Common.DataColumnMapping("Compare", "Compare"), New System.Data.Common.DataColumnMapping("Hits", "Hits"), New System.Data.Common.DataColumnMapping("GroupXpathTypeID", "GroupXpathTypeID")})})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT GroupXpathID, XpathName, XPath, Compare, Hits, GroupXpathTypeID FROM Group" & _
        "Xpath ORDER BY XpathName"
        Me.SqlSelectCommand1.Connection = Me.DBConnection
        '
        'GetXPathSqlCommand
        '
        Me.GetXPathSqlCommand.CommandText = "SELECT GroupXpathID, XpathName, XPath, Compare, Hits FROM GroupXpath WHERE (Group" & _
        "XpathID = @id)"
        Me.GetXPathSqlCommand.Connection = Me.DBConnection
        Me.GetXPathSqlCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int, 4, "GroupXpathID"))
        '
        'Status
        '
        Me.Status.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Status.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Status.Location = New System.Drawing.Point(848, 701)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(104, 20)
        Me.Status.TabIndex = 20
        Me.Status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Subs
        '
        Me.Subs.Location = New System.Drawing.Point(576, 8)
        Me.Subs.Name = "Subs"
        Me.Subs.Size = New System.Drawing.Size(136, 20)
        Me.Subs.TabIndex = 21
        Me.Subs.Text = "Include subdirectories"
        '
        'XPathForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(960, 726)
        Me.Controls.Add(Me.DBList)
        Me.Controls.Add(Me.Subs)
        Me.Controls.Add(Me.Status)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Progress)
        Me.Controls.Add(Me.AbortBtn)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.FileHits)
        Me.Controls.Add(Me.TotalFiles)
        Me.Controls.Add(Me.SampleDir)
        Me.Controls.Add(Me.Query)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.BrowseBtn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CompareType)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Hits)
        Me.Controls.Add(Me.TestBtn)
        Me.Controls.Add(Me.ResultList)
        Me.Name = "XPathForm"
        Me.Text = "NTB XPath feed selection creator/tester"
        CType(Me.Hits, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XPathDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private fileList As New ArrayList
    Private _abort As Boolean = False


    Private Sub BrowseBtn_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles BrowseBtn.Click

        Dim dlg As New FolderBrowserDialog()

        dlg.Description = "Select the folder to use as an XML file sample selection"
        dlg.SelectedPath = SampleDir.Text
        ' dlg.RestoreDirectory = True

        ' dlg.SelectedPath = SampleDir.Text

        Dim result As DialogResult = dlg.ShowDialog()

        If result = DialogResult.OK Then
            SampleDir.Text = dlg.SelectedPath
            GetFiles()
        End If

    End Sub

    Private Sub GetFiles()

        fileList.Clear()

        Try
            GetFiles(SampleDir.Text, fileList, Subs.Checked)
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & vbCrLf & ex.StackTrace)
        End Try

        TotalFiles.Text = fileList.Count
        Progress.Maximum = fileList.Count
    End Sub

    'Recursive Getfiles
    Private Sub GetFiles(ByVal f As String, ByRef l As ArrayList, Optional ByVal recurse As Boolean = False, Optional ByVal filter As String = "*.xml")

        If recurse Then
            Dim dir
            Dim dirs As String() = Directory.GetDirectories(f)
            For Each dir In dirs
                GetFiles(dir, l, recurse, filter)
            Next
        End If

        l.AddRange(Directory.GetFiles(f, filter))

    End Sub

    Private Sub XPathForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CompareType.SelectedIndex = 0
        Hits.Text = 1
        Dim today As DateTime = DateTime.Today
        Dim todayAndMonth = today.Year & "-" & today.Month.ToString().PadLeft(2, "0")
        Dim todaybackwards = today.Year & "-" & today.Month.ToString().PadLeft(2, "0") & "-" & today.Day.ToString().PadLeft(2, "0")

        SampleDir.Text = "\\bhutan\d$\XmlXsltRouteData\NITF-done\" & todayAndMonth & "\" & todaybackwards
        GetFiles()

        XPathAdapter.Fill(XPathDataSet)
        DBList.SelectedIndex = 1
        DBList.SelectedIndex = 0
    End Sub

    Private Function CheckXpath(ByVal xmlfile As String, ByRef intHits As Integer, ByRef kw As String, ByRef svc As String, ByRef sg As String, ByRef ug As String, ByRef stamp As String, ByRef err As Boolean) As Boolean

        Dim xmlDoc As XmlDocument = New XmlDocument
        Dim nodeList As XmlNodeList

        Dim intCompareHit As Integer
        Dim bFound As Boolean

        err = False
        intCompareHit = Hits.Text
        Dim strXpath As String = Query.Text

        Try
            xmlDoc.Load(xmlfile)
            nodeList = xmlDoc.SelectNodes(strXpath)
            intHits = nodeList.Count
        Catch ex As Exception
            MessageBox.Show(ex.Message & vbCrLf & vbCrLf & ex.StackTrace)
            err = True
            Return False
        End Try

        Try
            stamp = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='ntb-dato']/@content").Value
            kw = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='subject']/@content").Value
            svc = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBTjeneste']/@content").Value
            sg = xmlDoc.SelectSingleNode("/nitf/head/tobject/@tobject.type").Value
            ug = xmlDoc.SelectSingleNode("/nitf/head/tobject/tobject.property/@tobject.property.type").Value
        Catch ex As Exception
            stamp = "<none>"
            kw = "<none>"
            svc = "<none>"
            sg = "<none>"
            ug = "<none>"

            'MessageBox.Show(ex.Message & vbCrLf & vbCrLf & ex.StackTrace)
            'Return False
        End Try


        bFound = False

        Select Case CompareType.SelectedIndex
            Case 0
                If intHits > intCompareHit Then
                    bFound = True
                End If
            Case 1
                If intHits = intCompareHit Then
                    bFound = True
                End If
            Case 2
                If intHits < intCompareHit Then
                    bFound = True
                End If
            Case 3
                If intHits <> intCompareHit Then
                    bFound = True
                End If
        End Select

        Return bFound
    End Function

    Private Sub TestBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestBtn.Click

        ResultList.Items.Clear()
        FileHits.Text = 0
        Progress.Value = 0
        Status.Text = "Running... (0)"

        _abort = False

        AbortBtn.Enabled = True
        TestBtn.Enabled = False

        Dim stamp As String
        Dim kw As String
        Dim svc As String
        Dim sg As String
        Dim ug As String
        Dim file As String
        Dim hits As Integer
        Dim err As Boolean = False

        For Each file In fileList

            If _abort Then
                Status.Text = "Aborted"
                Exit For
            End If


            If CheckXpath(file, hits, kw, svc, sg, ug, stamp, err) Then

                Dim itm As ListViewItem = ResultList.Items.Add(kw)
                itm.SubItems.Add(stamp)
                itm.SubItems.Add(svc)
                itm.SubItems.Add(sg)
                itm.SubItems.Add(ug)
                itm.SubItems.Add(hits)
                itm.SubItems.Add(Path.GetFileName(file))
                itm.SubItems.Add(file)

                FileHits.Text += 1
            End If

            If err Then
                Exit For
            End If

            Progress.PerformStep()
            Status.Text = "Running... (" & Progress.Value & ")"
            Application.DoEvents()

        Next

        If (Status.Text <> "Aborted") Then Status.Text = "Done"
        AbortBtn.Enabled = False
        TestBtn.Enabled = True
    End Sub

    Private Sub AbortBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AbortBtn.Click
        _abort = True
    End Sub

    Private Sub XPathForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        _abort = True
    End Sub

    Private Sub DBList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DBList.SelectedIndexChanged
        Dim res As SqlClient.SqlDataReader
        Dim xpid As Integer = DBList.SelectedValue

        Try
            DBConnection.Open()
            GetXPathSqlCommand.Parameters("@id").Value = xpid

            res = GetXPathSqlCommand.ExecuteReader()
            res.Read()

            Query.Text = res("XPath")
            Hits.Text = res("Hits")

            Select Case res("Compare")
                Case "gt"
                    CompareType.SelectedIndex = 0
                Case "eq"
                    CompareType.SelectedIndex = 1
                Case "lt"
                    CompareType.SelectedIndex = 2
                Case "ne"
                    CompareType.SelectedIndex = 3
            End Select

            DBConnection.Close()
        Catch ex As Exception
            DBConnection.Close()
        End Try
    End Sub

    Private Sub Subs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Subs.CheckedChanged
        GetFiles()
    End Sub

    Private Sub ResultList_ItemActivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles ResultList.ItemActivate
        Dim st As ProcessStartInfo = New ProcessStartInfo

        st.FileName = ResultList.Items(ResultList.SelectedIndices(0)).SubItems(7).Text
        st.UseShellExecute = True
        st.CreateNoWindow = False
        st.WindowStyle = ProcessWindowStyle.Normal

        System.Diagnostics.Process.Start(st)

    End Sub
End Class

